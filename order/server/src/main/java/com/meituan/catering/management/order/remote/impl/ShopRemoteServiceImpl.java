package com.meituan.catering.management.order.remote.impl;

import com.meituan.catering.management.common.remote.BaseThriftRemoteService;
import com.meituan.catering.management.order.remote.ShopRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ShopRemoteServiceImpl extends BaseThriftRemoteService implements ShopRemoteService {

}
