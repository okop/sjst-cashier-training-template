package com.meituan.catering.management.shop.biz.model;

import com.meituan.catering.management.common.model.biz.BaseBO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 门店BO定义
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopBO extends BaseBO {

}