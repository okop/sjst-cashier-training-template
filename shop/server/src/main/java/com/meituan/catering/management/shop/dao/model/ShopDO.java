package com.meituan.catering.management.shop.dao.model;

import com.meituan.catering.management.common.model.dao.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 门店DO定义
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ShopDO extends BaseDO {

}